package com.gk_software.model;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import com.gk_software.exception.PosException;
import com.gk_software.util.Util;


/**
 * @author rbobak
 * Represents point of sale
 */
public class PosImpl implements POS {
	private int currentMoneyAmount;
	private ArrayList<Item> listOfAvailableSaleItems;
	private ArrayList<Item> listOfSoldItems;
	
	public PosImpl() {
		currentMoneyAmount = POS.MINIMAL_AMOUNT_FOR_CHANGE;
		listOfAvailableSaleItems = new ArrayList<Item>();
		listOfSoldItems = new ArrayList<Item>();
	}

	public int getCurrentMoneyAmount() {
		return currentMoneyAmount;
	}

	public List<Item> getListOfAvailbaleSaleItems() {
		return listOfAvailableSaleItems;
	}

	public void sellItem(Item item) throws PosException {
		if (!listOfAvailableSaleItems.contains((Item)item))
			throw new PosException("This item does not exist in the DB");
		currentMoneyAmount += item.getPrice();
		item.setPurchaseDate(Util.createDateWithDaysOffset(item.getItemId()));
		listOfSoldItems.add(item);
		listOfAvailableSaleItems.remove(item);
	}

	public void returnSoldItem(Item item) throws PosException {
		if (!listOfSoldItems.contains((Item)item)) {
			throw new PosException("This item does not exist in the DB");
		}
		
		listOfSoldItems.remove(item);
		item.setPurchaseDate(null);
		currentMoneyAmount -= item.getPrice();
		listOfAvailableSaleItems.add(item);
	}

	public void encashment() {
		if (currentMoneyAmount > POS.MINIMAL_AMOUNT_FOR_CHANGE)
			currentMoneyAmount = POS.MINIMAL_AMOUNT_FOR_CHANGE;
	}

	public void storeItems(List<Item> items) {
		listOfAvailableSaleItems = (ArrayList<Item>) items;
	}

	public String report(Timestamp from, Timestamp to) {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy hh:mm:ss"); 
		System.out.println("<<< REPORT >>>");
		System.out.println("From: " + simpleDateFormat.format(from));
		System.out.println("To: " + simpleDateFormat.format(to));
		
		StringBuilder strResult = new StringBuilder();
		if (listOfSoldItems.size() == 0)
			strResult.append("There are no sold items");
		
		for (Item item : listOfSoldItems) {
			if (item.getPurchaseDate().compareTo(from) >= 0 && item.getPurchaseDate().compareTo(to) <= 0)
				strResult.append("\n" + item);
		}
		
		return strResult.toString();
	}

}
