package com.gk_software.model;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;

/**
 * @author rbobak
 * Keeps information about the Item
 */
public class Item {
	private static int itemsCount = 0;
	
	private int itemId;
	private String name;
	private int price;
	private Timestamp purchaseDate;
	
	public Item(String name, int price) {
		this.itemId = getItemsCount();
		this.name = name;
		this.price = price;
	}

	private static int getItemsCount() {
		return ++itemsCount;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public Timestamp getPurchaseDate() {
		return purchaseDate;
	}

	public void setPurchaseDate(Timestamp purchaseDate) {
		this.purchaseDate = purchaseDate;
	}

	public int getItemId() {
		return itemId;
	}

	public static void setItemsCount(int itemsCount) {
		Item.itemsCount = itemsCount;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + itemId;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + price;
		result = prime * result + ((purchaseDate == null) ? 0 : purchaseDate.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Item other = (Item) obj;
		if (itemId != other.itemId)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (price != other.price)
			return false;
		if (purchaseDate == null) {
			if (other.purchaseDate != null)
				return false;
		} else if (!purchaseDate.equals(other.purchaseDate))
			return false;
		return true;
	}

	@Override
	public String toString() {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy hh:mm:ss"); 
		return "Item [itemId=" + itemId + ", name=" + name + ", price=" + price + ", purchaseDate=" + simpleDateFormat.format(purchaseDate)
				+ "]";
	}
}
