package com.gk_software.util;

import java.sql.Timestamp;
import java.util.Calendar;

/**
 * @author rbobak
 * Util is used for extra purposes
 */
public class Util {
	public static Timestamp createDateWithDaysOffset(int dayOffset) {
		Calendar calendar = Calendar.getInstance();
		int day = calendar.get(Calendar.DAY_OF_MONTH) - (10 - dayOffset);
		calendar.set(Calendar.DAY_OF_MONTH, day);
		
//		int year = calendar.get(Calendar.YEAR);
//		int month = calendar.get(Calendar.MONTH);
//		int day = calendar.get(Calendar.DAY_OF_MONTH) - (10 - dayOffset);
//		
//		int hour = calendar.get(Calendar.HOUR_OF_DAY);
//		int minute = calendar.get(Calendar.MINUTE);
//		int second = calendar.get(Calendar.SECOND);
		
		return new Timestamp(calendar.getTimeInMillis());
	}
}
