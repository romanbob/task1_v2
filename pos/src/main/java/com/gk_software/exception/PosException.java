package com.gk_software.exception;

public class PosException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public PosException(String message) {
		super(message);
	}
}
