package store;

import java.util.ArrayList;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.gk_software.exception.PosException;
import com.gk_software.model.Item;
import com.gk_software.model.POS;
import com.gk_software.model.PosImpl;

public class PosJunit3Test extends Assert {
	PosImpl pos = new PosImpl();
	
	private Item bmwX3;
	private Item bmwX7;
	private Item audiA5;
	
	@Before
	public void setUpPos() throws Exception {
		System.out.println("setUpPos");
		ArrayList<Item> listOfAvailableItems = new ArrayList<Item>();
		
		bmwX3 = new Item("BMW X3", 3);
		bmwX7 = new Item("BMW X7", 7);
		audiA5 = new Item("Audi A5", 5);
				
		listOfAvailableItems.add(bmwX3);
		listOfAvailableItems.add(bmwX7);
		listOfAvailableItems.add(audiA5);

		pos.storeItems(listOfAvailableItems);
	}
	
//	Init state	
	@Test
	public void testInitialMoneyAmount() {
		assertEquals(POS.MINIMAL_AMOUNT_FOR_CHANGE, pos.getCurrentMoneyAmount());
	}

	@Test
	public void testInitSizeOfAvailableSaleItemsList() throws PosException {
		assertEquals(3, pos.getListOfAvailbaleSaleItems().size());
	}

//	Purchase
	@Test
	public void testMoneyAmountAfterPurchase() throws PosException {
		pos.sellItem(bmwX7);
		assertEquals(10, pos.getCurrentMoneyAmount());
	}
	
	@Test
	public void testSizeOfAvailableSaleItemsListAfterItemPurchase() throws PosException {
		pos.sellItem(bmwX7);
		assertEquals(2, pos.getListOfAvailbaleSaleItems().size());
	}

//	Returning item
	@Test
	public void testMoneyAmountAfterItemReturning() throws PosException {
		pos.sellItem(bmwX7);
		pos.returnSoldItem(bmwX7);
		assertEquals(3, pos.getCurrentMoneyAmount());
	}

	@Test
	public void testSizeOfAvailableSaleItemsListAfterItemReturning() throws PosException {
		pos.sellItem(bmwX7);
		pos.returnSoldItem(bmwX7);
		assertEquals(3, pos.getListOfAvailbaleSaleItems().size());
	}
	
//	Encashment
	@Test
	public void testMoneyAmountAfterEncashment() throws PosException {
		pos.sellItem(bmwX7);
		pos.encashment();
		assertEquals(3, pos.getCurrentMoneyAmount());
	}
}
