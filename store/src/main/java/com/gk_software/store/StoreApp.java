package com.gk_software.store;

import java.util.ArrayList;

import com.gk_software.exception.PosException;
import com.gk_software.model.Item;
import com.gk_software.model.PosImpl;
import com.gk_software.util.Util;

/**
 * @author rbobak
 * Entry point of the store application
 */
public class StoreApp {
	public static void main(String args[]) throws PosException {
//		Create all items 
		ArrayList<Item> listOfAvailableItems = new ArrayList<Item>();
		Item bmwX3 = new Item("BMW X3", 3);
		Item bmwX7 = new Item("BMW X7", 7);
		Item audiA5 = new Item("Audi A5", 5);
		
		listOfAvailableItems.add(bmwX3);
		listOfAvailableItems.add(bmwX7);
		listOfAvailableItems.add(audiA5);
		
//		Creating and initializing point of sale 		
		PosImpl pos = new PosImpl();
		pos.storeItems(listOfAvailableItems);
		
//		Initial current amount and list of available items
		System.out.println("Initial amount is: " + pos.getCurrentMoneyAmount());
		System.out.println(pos.report(Util.createDateWithDaysOffset(1), Util.createDateWithDaysOffset(3)));
		
		
///////	First purchase
		System.out.println("\n<<< First purchase >>>");
		pos.sellItem(bmwX7);
		
//		Money amount and list of available items after 1st purchase
		System.out.println("Money amount after 1st purchase is: " + pos.getCurrentMoneyAmount());
		System.out.println(pos.report(Util.createDateWithDaysOffset(1), Util.createDateWithDaysOffset(3)));
		
///////	Returning first purchase
		pos.returnSoldItem(bmwX7);
//		Money amount and list of available items after 1st purchase
		System.out.println("\nMoney amount after returning 1st purchase: " + pos.getCurrentMoneyAmount());
		System.out.println(pos.report(Util.createDateWithDaysOffset(0), Util.createDateWithDaysOffset(5)));

		
///////	Encashment
		System.out.println("\n<<< Encashment >>>");
		pos.encashment();;
		
//		Money amount and list of available items after encashment
		System.out.println("Money amount after encashment: " + pos.getCurrentMoneyAmount());

///////	Second purchase
		System.out.println("\n<<< Second purchase >>>");
		pos.sellItem(audiA5);
		
//		Money amount and list of available items after 2nd purchase
		System.out.println("Money amount after 2nd purchase is: " + pos.getCurrentMoneyAmount());
		System.out.println(pos.report(Util.createDateWithDaysOffset(1), Util.createDateWithDaysOffset(3)));
	}
}
